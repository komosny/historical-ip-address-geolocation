# Free data


The free to use databases were removed due to [MaxMind policy](https://www.maxmind.com/en/geolite2/eula). This policy states that the MaxMind’s free data have to be deleted within 30 days after their update, that is, one cannot use old free databases. For the purpose of the work, other free location databases can be used, including their past versions. [IP2Location](https://www.ip2location.com/) is recommended. Contact the author for details.


# Corrections


Link-local IPv6 addresses were wrongly included in the ground-truth dataset. Therefore:


- The curve titled ‘IPv6 NA loc.considered’ in Figure 5 is not valid.
- The values titled ‘NA locations considered’ in Table 3 are not valid.


Other data and results are untouched.


There also is a typo in Table 5 – the last two lines show values for ‘IPv6’.
